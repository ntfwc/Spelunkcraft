# Spelunkcraft
Mod Discord Server:

https://discord.gg/CJhWB3F

Mod Trello:

https://trello.com/b/NNrIF0j7/spelunkcraft

This is a mod that brings elements and mechanics from Spelunky, such as Underground Jungles, Ice Caves, Olmec, etc. 
The last version of this mod was for 1.6.4 way back in 2013 so this 1.12.2 port is completely from scratch and very much incomplete, as a warning!
I may or may not maybe pulled/setup this GitLab correctly 
(probably regarding the gradlew stuff)
(not too familiar with gradle besides the basics of the decompworkspace shit and all) 
for use with GitHub Desktop + Eclipse/Idea
but with all the nessary source-code/java files here in the repo, 
you should be able to clone and help work on it in a Java IDE if you want.

Please keep note that most of the development changes occur on the unstable branch, and so things may be unfinished, have bugs, etc.
For bug reports, please submit them to the Issues part of the repo.

On a small note, keep note that the online commit-timestamps are UTC based while my local timezone is Pacific Standard Time.