package ladestitute.spelunkcraft;

import ladestitute.spelunkcraft.proxy.CommonProxy;
import ladestitute.spelunkcraft.util.Reference;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

//Developer notes and reminders:
//To add new items, edit ModItems.java + en_us.lang and also accordingly implement their corresponding .json files and textures
//I will work on unique classes for blocks later, in order to define things like hardness, blast resistance, etc
//A simple singular class for blocks/items is just currently existing
//so I can get this mod's workspace up and running, get it on Gitlab, etc
//Not much here yet, don't mind the small Main class here

//Grab the MODID/NAME/VERSION from the Reference class in util
@Mod(modid = Reference.Mod_ID, name = Reference.NAME, version = Reference.VERSION)
public class Main 

    {

	//The instance for the mod, for reference such as later opening GUIs
	@Instance
	public static Main instance;
	
	//Get the paths for the Common and Client proxies
	@SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.COMMON_PROXY_CLASS)
	public static CommonProxy proxy;
	
	//Event handlers for PreInit, Init, and PostInit
	@EventHandler
	public static void PreInit(FMLPreInitializationEvent event)
	{
	
	}
	
	@EventHandler
	public static void init(FMLInitializationEvent event)
	{
	
	}
	
	@EventHandler
	public static void PostInit(FMLPostInitializationEvent event)
	{
	
	}
}