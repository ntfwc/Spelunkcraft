package ladestitute.spelunkcraft.util;

//References here, for use in other parts of the mod's code.
public class Reference {
public static final String Mod_ID = "spelunkcraft";
public static final String NAME = "Spelunkcraft";
public static final String VERSION ="0.1";
public static final String ACCEPTED_VERSIONS = "1.12.2";
public static final String CLIENT_PROXY_CLASS = "ladestitute.spelunkcraft.proxy.ClientProxy";
public static final String COMMON_PROXY_CLASS = "ladestitute.spelunkcraftproxy.CommonProxy";
}