package ladestitute.spelunkcraft.blocks;

import ladestitute.spelunkcraft.Main;
import ladestitute.spelunkcraft.init.ModBlocks;
import ladestitute.spelunkcraft.init.ModItems;
import ladestitute.spelunkcraft.util.IHasModel;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;

public class BlockBase extends Block implements IHasModel 
{
    public BlockBase(String name, Material material)
    {
    	//Set both Unlocalized and Registry names as well as set the Creative tab (custom tabs will come soon!)
    	super(material);
    	setUnlocalizedName(name);
    	setRegistryName(name);
    	setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
    	
    	//Add each block and set Registry names
    	ModBlocks.BLOCKS.add(this);
    	ModItems.ITEMS.add(new ItemBlock(this).setRegistryName(this.getRegistryName()));
    }

	@Override
	public void registerModels() 
	{
		//Register the ItemRenderer
		Main.proxy.registerItemRenderer(Item.getItemFromBlock(this), 0, "inventory");
	}
}