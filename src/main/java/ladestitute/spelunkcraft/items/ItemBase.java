package ladestitute.spelunkcraft.items;

import ladestitute.spelunkcraft.Main;
import ladestitute.spelunkcraft.init.ModItems;
import ladestitute.spelunkcraft.util.IHasModel;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemBase extends Item implements IHasModel 
{

	public ItemBase(String name)
	{
	//Set both Unlocalized and Registry names as well as set the Creative tab (custom tabs will come soon!)
	setUnlocalizedName(name);
	setRegistryName(name);
	setCreativeTab(CreativeTabs.MATERIALS);
	
	//Add each item
	ModItems.ITEMS.add(this);
	}
	
	@Override
	public void registerModels() 
	{
		//Register ItemRenderer
		Main.proxy.registerItemRenderer(this, 0, "inventory");
		
	}

}
