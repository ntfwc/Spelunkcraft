package ladestitute.spelunkcraft.init;

import java.util.ArrayList;
import java.util.List;

import ladestitute.spelunkcraft.blocks.BlockBase;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class ModBlocks 
{
    public static final List<Block> BLOCKS = new ArrayList<Block>();
    
    public static final Block RUBY_ORE = new BlockBase("ruby_ore", Material.ROCK);
    public static final Block SAPPHIRE_ORE = new BlockBase("sapphire_ore", Material.ROCK);
}

