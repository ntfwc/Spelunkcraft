package ladestitute.spelunkcraft.init;

import java.util.ArrayList;
import java.util.List;

import ladestitute.spelunkcraft.items.ItemBase;
import net.minecraft.item.Item;

public class ModItems 
{
 public static final List<Item> ITEMS = new ArrayList<Item>();
 
 public static final Item RUBY_SMALL = new ItemBase("ruby_small");
 public static final Item RUBY_LARGE = new ItemBase("ruby_large");
 public static final Item SAPPHIRE_SMALL = new ItemBase("sapphire_small");
 public static final Item SAPPHIRE_LARGE = new ItemBase("sapphire_large");
}
